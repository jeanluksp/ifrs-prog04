package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    Scene scene1, scene2;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Botão 1
        Label label1 = new Label("Cena 1");
        Button button1 = new Button("Ir para Cena 2");
        button1.setOnAction(e -> primaryStage.setScene(scene2));

        // Layout 1 - vertical
        VBox layout1 = new VBox(20);
        layout1.getChildren().addAll(label1, button1);
        scene1 = new Scene(layout1, 200, 200);


        // Botão 2
        Button button2 = new Button("Voltar para Cena 1");
        button2.setOnAction(e -> primaryStage.setScene(scene1));

        // Layout 2
        StackPane layout2 = new StackPane();
        layout2.getChildren().add(button2);
        scene2 = new Scene(layout2, 600, 300);

        // Exibe a Cena 1
        primaryStage.setScene(scene1);
        primaryStage.setTitle("Título da Janela");
        primaryStage.show();
    }
}
