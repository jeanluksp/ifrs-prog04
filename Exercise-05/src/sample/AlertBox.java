package sample;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlertBox {

    public static void display(String title, String message) {
        // TODO (1) - Cria um stage chamado 'window'

        // TODO (2) - Configurar o bloqueio de eventos para outras janelas utilizando 'window.initModality'

        // TODO (3) - Setar título (title) da janela
        // TODO (4) - Setar largura mínima na janela(250)

        Label label = new Label();
        label.setText(message);
        Button closeButton = new Button("Fechar alerta");
        closeButton.setOnAction(e -> window.close());

        VBox layout = new VBox(10);
        layout.getChildren().addAll(label, closeButton);
        layout.setAlignment(Pos.CENTER);

        // Exibe a janela e aguarda o fechamento antes de retornar
        Scene scene = new Scene(layout);

        // TODO (5) - Setar cena na janela
        // TODO (6) - Chamar o método 'window.showAndWait()'

        window.setScene(scene);
        window.showAndWait();
    }

}
