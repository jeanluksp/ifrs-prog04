package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.controller.LoginController;
import sample.controller.MainController;
import sample.model.Person;

import java.io.IOException;

public class Main extends Application {

    private Stage primaryStage;
    // TODO (4) - Mude o tipo da ObservableList para 'Person'
    private ObservableList<T> personData = FXCollections.observableArrayList();

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        Scene root = loadLoginScene();

        primaryStage.setScene(root);
        primaryStage.show();
    }

    private Scene loadLoginScene() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/LoginView.fxml"));

        Parent root = loader.load();
        Scene scene = new Scene(root);

        ((LoginController) loader.getController()).setMain(this);

        return scene;
    }

    private Scene loadMainScene() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        // TODO (3) - Carregue o fxml MainView
        loader.setLocation(getClass().getResource("view/MainView.fxml"));

        Parent root = loader.load();
        Scene scene = new Scene(root);

        ((MainController) loader.getController()).setMain(this);

        return scene;
    }

    public void handleLogin(String text, String passwordFieldText) {
        // load data from source
        // TODO (6) - Adicione mais elementos à ObservableList personData
        personData.add(new Person("Angelo", "Fabris"));
        personData.add(new Person("Arlei", "Duarte"));
        personData.add(new Person("Christian", "Theobald"));
        personData.add(new Person("Gabriel", "Siqueira"));
        personData.add(new Person("Igor", "Kalb"));
        personData.add(new Person("Jean", "Pereira"));
        personData.add(new Person("Pedro", "Tessaro"));
        personData.add(new Person("William", "Grosseli"));

        try {
            // TODO (1) - Chame o método loadMainScene
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleLogout() {
        // TODO (5) - Limpe a ObservableList personData

        try {
            // TODO (2) - Chame o método loadLoginScene
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public ObservableList<Person> getPersonData() {
        return personData;
    }

    public void setPersonData(ObservableList<Person> personData) {
        this.personData = personData;
    }
}
